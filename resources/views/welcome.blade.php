<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>DocLine</title>

    {{-- Bootstarp link --}}

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    {{-- jquery link --}}
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

    <!--Google font file-->
    <link
        href="https://fonts.googleapis.com/css?family=Arvo|Lexend+Tera|Montserrat|Noto+Serif|Nunito|PT+Sans|Raleway|Varela+Round&display=swap"
        rel="stylesheet">

    <!--css file-->
    <link rel="stylesheet" href="/css/main.css">

    {{--  font awsome link  --}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        * {
            margin: 0;
            padding: 0;
        }

        .navbar {
            font-family: "Raleway";
            font-weight: 500;
            font-size: 18px;
            letter-spacing: 2px;
        }

        .navbar-brand {
            font-family: "Arvo" !important;
            font-weight: bolder;
            color: #0080ff !important;
            font-size: 30px;
        }

        .nav-item .nav-link {
            color: #333333 !important;
            transition: transform 0.3s ease-out;
        }

        .nav-item {
            padding: 10px;
        }

        .navbar {
            background-color: white !important;
            /* box-shadow: 0 15px 15px -15px #808080; */
            box-shadow: 0 10px 10px -10px #404040;
        }

        .nav-item a:hover {
            /* border-bottom: 1px solid #0080ff; */
            color: #0080ff !important;
            transform: scale(1.2);

        }

        * {
            margin: 0;
            padding: 0;
        }


        /* carsoule css */

        .slider {
            margin-top: 0px;
            /* border-radius: 20px !important; */
        }

        .carousel-item img {
            height: 520px;
            width: 100vw;
            border-radius: 10px !important;
        }


        /* mobile css */

        .frame img {
            height: 600px;
        }

        /* mobile heading */

        .mobile {
            margin-top: 7% !important;
        }

        .title {
            font-size: 40px;
            font-family: "Raleway";
            font-weight: bolder;
            line-height: 45px;
            /* transition: transform 0.7s ease-out; */
        }

        .sub-title {
            color: #737373;
            letter-spacing: 1px;
            font-family: "Raleway";
            padding-top: 10px;
        }

        /* .mob-content{
            height: 600px;
            width:700px;
        } */

        /* how it works */

        .work {
            font-size: 50px;
            font-family: "Raleway";
            font-weight: bolder;
        }

        .des-title {
            font-size: 20px;
            font-family: "Raleway";
            font-weight: bold;
            letter-spacing: 1px;
        }

        .des {
            color: #111111;
            font-family: "Raleway";
            letter-spacing: 1px;
        }

        .first {
            padding: 10px 10px;
            border: 1px solid #ffffff;
        }

        .first:hover {
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.2);
            border-radius: 5px;
            color: #0080ff;
        }


        .second {
            padding: 10px 10px;
            border: 1px solid #ffffff;
        }

        .second:hover {
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.2);
            border-radius: 5px;
            color: #0080ff;
        }


        .third {
            padding: 10px 10px;
            border: 1px solid #ffffff;
        }

        .third:hover {
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.2);
            border-radius: 5px;
            color: #0080ff;
        }


        .fourth {
            padding: 10px 10px;
            border: 1px solid #ffffff;
        }

        .fourth:hover {
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.2);
            border-radius: 5px;
            color: #0080ff;
        }

        /* Specialities Treated At DocsApp */
        .specialities-title {
            font-size: 40px;
            font-family: "Raleway";
            font-weight: bolder;
        }

        .card img {
            height: 100px;
            width: 100px;
        }

        .card {
            border: 1px solid #ffffff;
            background-color: #f2f2f2;
        }


        .card:hover .card-title {
            color: #0080ff;
            font-weight: bolder !important;
        }

        .card-title {
            font-size: 15px !important;
            font-family: "Raleway";
            letter-spacing: 1px;
        }

        .card-first {
            transition: 0.5s ease-in-out;
            border-radius: 10px !important;
        }

        .card-first:hover {
            transform: scale(1.2);
            z-index: 1;
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.4);
            border-radius: 10px !important;
        }

        .card-second {
            transition: 0.5s ease-in-out;
            border-radius: 10px !important;
        }

        .card-second:hover {
            transform: scale(1.2);
            z-index: 1;
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.4);
            border-radius: 10px !important;
        }

        .card-third {
            transition: 0.5s ease-in-out;
            border-radius: 10px !important;
        }

        .card-third:hover {
            transform: scale(1.2);
            z-index: 1;
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.4);
            border-radius: 10px !important;
        }

        .card-fourth {
            transition: 0.5s ease-in-out;
            border-radius: 10px !important;
        }

        .card-fourth:hover {
            transform: scale(1.2);
            z-index: 1;
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.4);
            border-radius: 10px !important;
        }


        .card-fifth {
            transition: 0.5s ease-in-out;
            border-radius: 10px !important;
        }

        .card-fifth:hover {
            transform: scale(1.2);
            z-index: 1;
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.4);
            border-radius: 10px !important;
        }


        .card-sixth {
            transition: 0.5s ease-in-out;
            border-radius: 10px !important;
        }

        .card-sixth:hover {
            transform: scale(1.2);
            z-index: 1;
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.4);
            border-radius: 10px !important;
        }

        .card-seventh {
            transition: 0.5s ease-in-out;
            border-radius: 10px !important;
        }

        .card-seventh:hover {
            transform: scale(1.2);
            z-index: 1;
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.4);
            border-radius: 10px !important;
        }


        .card-eight {
            transition: 0.5s ease-in-out;
            border-radius: 10px !important;
        }

        .card-eight:hover {
            transform: scale(1.2);
            z-index: 1;
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.4);
            border-radius: 10px !important;
        }

        .card-group .card {
            margin: 0px 5px;
        }


        /* our expert online doctor */
        .part3 {
            /*height: 80%;*/
            background-color: #0090FF;
        }

        .part3 .carousel {
            width: 100%;
        }

        .part3 .card .title {
            font-size: 1.2em;
        }

        .part3 .card .text {
            font-size: 0.9em;
        }

        .part3 .card .fa {
            color: #d9d9d9;
        }

        .part3 .card .checked {
            color: gold;
        }

        .part3 .card {
            margin-bottom: 7%;
        }

        .userr {
            /*background-color: pink;*/
            width: 100%;
            padding-top: 1%;
            font-size: 1.7em;
            text-align: center;
            font-weight: bold;
            /*font-family: "Ubuntu";*/
            color: #fff;
        }


        /* We Choose Only Best Doctors For You */

        .choose {
            font-size: 40px;
            font-family: "Raleway";
            font-weight: bolder;
        }

        /* doctor image css */
        .doctor {
            margin-top: 20px;
        }

        .pages {
            margin-top: 22px;
        }

        .certificate {
            margin-top: 20px;
        }

        .head {
            font-size: 17px;
            font-weight: bold;
            font-family: "Raleway" !important;
        }

        .content {
            font-size: 14px;
            color: #737373;
            font-family: "Raleway" !important;
            letter-spacing: 1px;
        }

        /* why choose docsApp */
        .docline-title {
            font-size: 40px;
            font-family: "Raleway";
            font-weight: bolder;
        }

        .docline {
            border: 1px solid #ffffff;
            height: 100px;
        }

        .doc a {
            font-family: "Raleway";
            letter-spacing: 1px;
            border: 1px solid #ffffff !important;
        }

        .doc a .active {
            border: 1px solid black !important;
        }

        .doc a:hover {
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.2);
            border-radius: 5px;
            color: #0080ff;
        }

        .doc .active {
            border-bottom: 1px solid #0080ff !important;
            box-shadow: 0px 0px 7px 3px rgba(145, 145, 145, 0.2);
            border-radius: 5px;
            color: #0080ff;
        }


        /* meet our founder */
        .mid3 {
            /*background-color: pink;*/
            width: 100%;
            margin-top: 3%;
            font-size: 40px;
            text-align: center;
            font-weight: bold;
            font-family: "Raleway";
            letter-spacing: 1px;
        }

        .part5 .row {
            width: 100%;
        }

        .part5 .card {
            border: 1px solid #ffffff !important;
            background-color: #ffffff !important;
            /*box-shadow: 0px 0px 7px 0px rgba(128,128,128,0.5);*/
        }

        .part5 .card .founderimg {
            border-radius: 50%;
        }

        .part5 .card .card-header {
            background-color: #fff;
            border-bottom: 0;
        }

        .part5 .card .foundername {
            font-size: 1.3em;
            font-family: "Raleway";
            color: #000;
            letter-spacing: 1px;
        }

        .part5 .card .foundertext {
            font-size: 1em;
            font-family: "Raleway";
            color: #000;
            letter-spacing: 1px;
        }

        .part5 .card .founderdetail {
            font-size: 0.9em;
            width: 80%;
            font-family: "Raleway";
            text-align: justify;
            color: #737373;
            margin-top: 3%;
            letter-spacing: 1px;
        }

        /* our team */
        .part6 .row {
            width: 100%;
        }

        .part6 .row .teamimg {
            height: 80%;
            width: 90%;
            margin-top: 9%;
        }

        .part6 .teamtext1 {
            font-size: 2em;
            font-family: "Raleway";
            font-weight: bold;
            margin-top: 11%;
        }

        .part6 .teamtext2 {
            font-size: 1em;
            color: #737373;
            font-family: "Raleway";
            letter-spacing: 1px;
        }

        .teamimg {
            border-radius: 10px;
            box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
        }

        .part6 .row .btn {
            width: 40%;
            background-color: #0090FF;
            color: #fff;
            border: 3px solid #0090FF;
        }

        .part6 .row .btn:hover {
            animation: pulse 1s ease-in-out 2 both;
        }

        @keyframes pulse {
            0% {
                transform: scale(1);
            }
            50% {
                transform: scale(0.9);
            }
            100% {
                transform: scale(1);
            }
        }

        /* footer */
        .part7 {
            width: 100%;
            /*background-color: #001d33;*/
            background-color: rgba(0, 29, 51, 0.9);
        }

        .lastname {
            font-family: "Raleway";
            letter-spacing: 1px;
        }

        .lasttext {
            font-family: "Raleway";
            letter-spacing: 1px;
        }

        .part7 .row {
            width: 100%;
        }

        .part7 .feedback {
            width: 100%;
            margin-top: 4%;
            padding-left: 10%;
        }

        .part7 .last .feedback .lastname {
            font-size: 2em;
            color: #fff;
            margin-left: 0%;
            font-weight: bold;
        }

        .part7 .last .feedback .lasttext {
            font-size: 1em;
            color: #ccc;
            margin-left: 0%;
        }

        .part7 .form {
            margin-left: 5%;
            padding-top: 3%;
            font-size: 0.8em;
        }

        .part7 .form .name {
            width: 40%;
            padding: 2%;
            padding-left: 2%;
            margin-right: 3%;
        }

        .part7 .form .email {
            width: 40%;
            padding: 2%;
            padding-left: 2%;
        }

        .part7 .form .msg {
            width: 83.5%;
            padding: 2%;
            padding-left: 2%;
        }

        .part7 .form .btn {
            width: 83.5%;
            background-color: rgba(0, 29, 51, 0.2);
            color: #fff;
            border: 2px solid #fff;
            font-size: 1.2em;
        }

        .part7 .lastline {
            color: #ccc;
        }

        hr {
            background-color: #a6a6a6;
        }

        .part7 .lastlink a {
            text-decoration: none;
            color: #f2f2f2;
            margin-top: 1%;
        }

        .part7 .copyright {
            text-align: center;
        }


        @media only screen and (max-width: 600px) {

            /* navbar */
            .navbar-nav {
                display: flex;
                flex-direction: column;
                align-items: center;
            }

            .nav-item .nav-link {
                color: #333333 !important;
                transition: transform 0.3s ease-out;
            }

            .nav-item a:hover {
                /* border-bottom: 1px solid #0080ff; */
                color: #0080ff !important;
                transform: scale(1.3);

            }

            .navbar-nav {
                display: flex;
                flex-direction: column;
                align-items: center;
            }

            .nav-item .nav-link {
                color: #333333 !important;
                transition: transform 0.3s ease-out;
            }

            .nav-item a:hover {
                /* border-bottom: 1px solid #0080ff; */
                color: #0080ff !important;
                transform: scale(1.3);

            }

            /* carousal */
            .carousel-item img {
                height: 330px;
                width: 100vw;
            }


            .frame img {
                height: 500px;
                width: 360px;
            }

            /* mobile content */
            .title {
                font-size: 30px;
                text-align: center;
            }

            .sub-title {
                font-size: 18px;
                text-align: center;
            }

            .mob-content {
                height: 300px;
                width: 100vw;
            }

            /* specialities-title */
            .specialities-title {
                font-size: 30px !important;
                text-align: center;
            }

            .card-title {
                font-weight: bold;
            }

            .card-first {
                transition: none;
                border-radius: 10px !important;
                background-color: #ffffff !important;
                color: #0080ff;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                margin-bottom: 15px !important;
            }

            .card-first:hover {
                transform: scale(1);
                z-index: 1;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                border-radius: 0px;

            }

            .card-second {
                transition: none;
                border-radius: 10px !important;
                background-color: #ffffff !important;
                color: #0080ff;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                margin-bottom: 15px !important;
            }

            .card-second:hover {
                transform: scale(1);
                z-index: 1;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                border-radius: 0px;
            }

            .card-third {
                transition: none;
                border-radius: 10px !important;
                background-color: #ffffff !important;
                color: #0080ff;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                margin-bottom: 15px !important;
            }

            .card-third:hover {
                transform: scale(1);
                z-index: 1;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                border-radius: 0px;
            }


            .card-fourth {
                transition: none;
                border-radius: 10px !important;
                background-color: #ffffff !important;
                color: #0080ff;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                margin-bottom: 15px !important;
            }

            .card-fourth:hover {
                transform: scale(1);
                z-index: 1;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                border-radius: 0px;
            }


            .card-fifth {
                transition: none;
                border-radius: 10px !important;
                background-color: #ffffff !important;
                color: #0080ff;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                margin-bottom: 15px !important;
            }

            .card-fifth:hover {
                transform: scale(1);
                z-index: 1;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                border-radius: 0px;
            }

            .card-sixth {
                transition: none;
                border-radius: 10px !important;
                background-color: #ffffff !important;
                color: #0080ff;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                margin-bottom: 15px !important;
            }

            .card-sixth:hover {
                transform: scale(1);
                z-index: 1;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                border-radius: 0px;
            }

            .card-seventh {
                transition: none;
                border-radius: 10px !important;
                background-color: #ffffff !important;
                color: #0080ff;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                margin-bottom: 15px !important;
            }

            .card-seventh:hover {
                transform: scale(1);
                z-index: 1;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                border-radius: 0px;
            }

            .card-eight {
                transition: none;
                border-radius: 10px !important;
                background-color: #ffffff !important;
                color: #0080ff;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                margin-bottom: 15px !important;
            }

            .card-eight:hover {
                transform: scale(1);
                z-index: 1;
                box-shadow: 0px 0px 5px 1px rgba(145, 145, 145, 0.4);
                border-radius: 0px;
            }

            /* how it works */
            .work {
                font-size: 35px;
            }

            /* why choose docs App */
            .choose {
                font-size: 30px !important;
            }

            .dekhavu {
                display: none !important;
            }

            .fast-doctor img {
                height: 250px !important;
                width: 250px !important;
                border-radius: 50% !important;
            }

            .title {
                font-size: 20px !important;
                text-align: center !important;
                line-height: 25px !important;
                margin-top: 10px !important;
            }

            .sub-title {
                font-size: 16px !important;
                margin-bottom: 5px !important;
            }

            .docline-title {
                font-size: 30px !important;
                align-items: center;
            }

        }

        @media only screen and (min-width: 992px) {
            .santadvu {
                display: none !important;
            }
        }

    </style>
</head>
<body>
<div>
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">


        {{--        <div>--}}
        {{--            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">--}}
        {{--                <a class="navbar-brand" href="#">DocLine</a>--}}
        {{--                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor03"--}}
        {{--                        aria-controls="navbarColor03" aria-expanded="false" aria-label="Toggle navigation">--}}
        {{--                    <span class="navbar-toggler-icon"></span>--}}
        {{--                </button>--}}

        {{--                <div class="collapse navbar-collapse justify-content-end" id="navbarColor03">--}}
        {{--                    <ul class="navbar-nav">--}}
        {{--                        <li class="nav-item {{ Request::path() ==  'login' ? 'active' : ''  }}">--}}
        {{--                            <a class="nav-link" href="#">Login</a>--}}
        {{--                        </li>--}}
        {{--                        <li class="nav-item {{ Request::path() ==  'sign up' ? 'active' : ''  }}">--}}
        {{--                            <a class="nav-link" href="/signup">Sing up</a>--}}
        {{--                        </li>--}}
        {{--                        <li class="nav-item {{ Request::path() ==  'home' ? 'active' : ''  }}">--}}
        {{--                            <a class="nav-link" href="/home">Home</a>--}}
        {{--                        </li>--}}
        {{--                        <li class="nav-item {{ Request::path() ==  'about' ? 'active' : ''  }}">--}}
        {{--                            <a class="nav-link" href="/about">About us</a>--}}
        {{--                        </li>--}}
        {{--                        <li class="nav-item {{ Request::path() ==  'contact' ? 'active' : ''  }}">--}}
        {{--                            <a class="nav-link" href="/contact">Contact</a>--}}
        {{--                        </li>--}}
        {{--                    </ul>--}}
        {{--                </div>--}}
        {{--            </nav>--}}
        {{--        </div>--}}


        {{-- mobile gif code goes here --}}

        <div class="mobile">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="frame">
                            <img src="/assets/img/chat2.gif" alt="">
                        </div>
                    </div>
                    <div class="col-sm-6 mob-content d-flex align-items-center">
                        <div class="">
                            <div class="title">Talk to Doctor Online on Private Chat & Call 24 x 7</div>
                            <div class="sub-title">Get Online Doctor Consultation in 30 minutes.Recover at <br>home On
                                Time
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>

        {{-- carsole goes here --}}

        <div class="slider container">
            <div id="my-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="/assets/img/xyz.jpg" alt="">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/assets/img/xyz2.jpg" alt="">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="/assets/img/xyz4.jpg" alt="">

                    </div>
                    {{-- <div class="carousel-item">
                        <img class="d-block w-100" src="/image/xyz5.jpg" alt="">
                         <div class="carousel-caption d-none d-md-block">
                            <h5>DocLine</h5>
                            <p>medicine</p>
                        </div>
                    </div> --}}
                    {{-- <div class="carousel-item">
                        <img class="d-block w-100" src="/image/xyz3.jpg" alt="">
                    </div> --}}
                </div>
                <a class="carousel-control-prev" href="#my-carousel" data-slide="prev" role="button">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#my-carousel" data-slide="next" role="button">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <br>
        <br>

        {{-- how it works --}}

        <div class="d-flex justify-content-center">
            <div class="work">How it works</div>
        </div>
        <br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-2">

                </div>
                <div class="col-sm-4">
                    <div class="description">
                        <div class="first">
                            <div class="des-title">Ask your Health Questions</div>
                            <div class="des">Choose gender and age of patient, Enter the details of health problem and
                                submit
                            </div>
                        </div>

                        <div class="second">
                            <div class="des-title">Get Online Doctor in Minutes</div>
                            <div class="des">A Specialist Doctor for your health problem will be appointed to you in
                                minutes. You can see user ratings, reviews and credentials of the assigned doctor
                            </div>
                        </div>

                        <div class="third">
                            <div class="des-title">Pay Doctor Consultation Fee Online</div>
                            <div class="des">You can pay the consultation fee through Credit card, Debit card,
                                Netbanking, E-Wallet. If your case cannot be treated online or if you are not satisfied
                                with your consultation, you will get 100% refund
                            </div>
                        </div>

                        <div class="fourth">
                            <div class="des-title">Get Diagnosis and Prescription</div>
                            <div class="des">You can start your consultation with your doctor via chat or call. After
                                the consultation is complete, the doctor will send you a prescription.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">


                </div>
                <div class="col-sm-2">

                </div>
            </div>
        </div>


        {{-- Specialities Treated At DocsApp --}}

        <br>
        <hr>
        <br>
        <div class="d-flex justify-content-center">
            <div class="specialities-title">Specialities Treated At DocsApp</div>
        </div>

        <br>
        <br>
        <div class="container-fluid">
            <div class="col-sm-12">
                <div class="card-group">

                    <div class="card card-first">
                        <img class="card-img-top mx-auto" src="/image/specialities/medicine.png" alt="">
                        <div class="card-body text-center">
                            <h5 class="card-title">Medicine</h5>
                        </div>
                    </div>

                    <div class="card card-second">
                        <img class="card-img-top mx-auto" src="/image/specialities/gynecology.jpg" alt="">
                        <div class="card-body text-center">
                            <h5 class="card-title">Gynecology</h5>
                        </div>
                    </div>

                    <div class="card card-third">
                        <img class="card-img-top mx-auto" src="/image/specialities/sexology.png" alt="">
                        <div class="card-body text-center">
                            <h5 class="card-title">Sexology</h5>
                        </div>
                    </div>

                    <div class="card card-fourth">
                        <img class="card-img-top mx-auto" src="/image/specialities/psychiatry.jpg" alt="">
                        <div class="card-body text-center">
                            <h5 class="card-title">Psychiatry</h5>
                        </div>
                    </div>

                    <div class="card card-fifth">
                        <img class="card-img-top mx-auto" src="/image/specialities/pregnant.png" alt="">
                        <div class="card-body text-center">
                            <h5 class="card-title">Pregnancy <br> Issues</h5>
                        </div>
                    </div>

                    <div class="card card-sixth">
                        <img class="card-img-top mx-auto" src="/image/specialities/paediatrics.png" alt="">
                        <div class="card-body text-center">
                            <h5 class="card-title">Paediatrics</h5>
                        </div>
                    </div>

                    <div class="card card-seventh">
                        <img class="card-img-top mx-auto" src="/image/specialities/weight.png" alt="">
                        <div class="card-body text-center">
                            <h5 class="card-title">Weight<br>Management</h5>
                        </div>
                    </div>

                    <div class="card card-eight">
                        <img class="card-img-top mx-auto" src="/image/specialities/dermatology.png" alt="">
                        <div class="card-body text-center">
                            <h5 class="card-title">Darmatology</h5>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <br>
        <br>
        <hr>
        <br>
        {{--  our expert online doctor  --}}
        <div class="part3">
            <div class="userr">
                <p>User Reviews</p>
            </div>

            <!-- carousel starts  -->
            <div class="carousel" id="mycarousel" data-ride="carousel">
                <div class="carousel-inner">
                    <!--  1st carousel item starts -->
                    <div class="carousel-item active">
                        <!--  Making of card  -->
                        <div class="container">
                            <div class="row">

                                <div class="col-md-4 col-lg-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <span class="title font-weight-bold">Mehul Vamja</span><br>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <br>
                                            <span class="text">It is a nice good website. Here anyone can book medical chekup appointment without any problem.</span>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <span class="title font-weight-bold">Sanket Beladiya</span><br>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <br>
                                            <span class="text">From this website i am able to find particular hospital's contact number and can contact hospital for information.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <span class="title font-weight-bold">Pujan Vachhani</span><br>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                            <br>
                                            <span class="text">Feature of booking leb-test is nice because, now i don't have to stand in the waiting line. That will save your time.</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--  Ending of card  -->
                    </div>
                    <!--  1st carousel item ends -->

                    <!--  2nd carousel item starts -->
                    <div class="carousel-item">
                        <!--  Making of card  -->
                        <div class="container">
                            <div class="row">

                                <div class="col-sm-12 col-lg-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <span class="title font-weight-bold">Yash Godhasara</span><br>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                            <br>
                                            <span class="text">I can order medicine online from anyplace. So i don't have to go at medical store. So it save my time.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <span class="title font-weight-bold">Manav Gajera</span><br>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <br>
                                            <span class="text">Appointment booking facility and medicine order facility provides security in payment. You can do transaction without any fear.</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <span class="title font-weight-bold">Sagar Donga</span><br>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star"></span>
                                            <span class="fa fa-star"></span>
                                            <br>
                                            <span class="text">This website gives so good health tips. So person can read about different methods and also take first-aid treatment.</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--  Ending of card  -->
                    </div>
                    <!--  2nd carousel item ends -->
                </div>

            </div>
            <!-- carousel ends  -->
        </div>
        <!-- part3 ends  -->


        <br>
        <br>


        {{-- choose doctor --}}
        <div class="d-flex justify-content-center">
            <div class="choose text-center">We Choose Only Best Doctors For You</div>
        </div>
        <br>
        <br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3 mx-auto">
                    <div class="image text-center">
                        <img src="/image/choosedoctor/trophy1.png" height="100px" width="100px" alt="">
                    </div>
                    <br>
                    <div class="text-center">
                        <div class="head">Shortlisting Doctor</div>
                        <div class="content">We shortlist doctors based on qualification, experience & credentials</div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="image text-center">
                        <img src="/image/choosedoctor/doctor.jpg" height="100px" width="80px" alt="">
                    </div>

                    <div class="doctor text-center">
                        <div class="head">Doctor Interview</div>
                        <div class="content">DocsApp medical Panel will have an interview with the Doctors before making
                            him DocsApp Doctor
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="image text-center">
                        <img src="/image/choosedoctor/page.png" height="100px" width="100px" alt="">
                    </div>
                    <div class="pages text-center">
                        <div class="head">Physical Verification</div>
                        <div class="content">The selected Doctor is verified physically by taking feedback from
                            pharmacies and local people
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="image text-center">
                        <img src="/image/choosedoctor/certification.jpg" height="100px" width="120px" alt="">
                    </div>
                    <div class="certificate text-center">
                        <div class="head">Online Consultation Certification</div>
                        <div class="content">Though the Doctor is expert in his field, as online consultation is new to
                            Doctor, DocsApp gives an Online Consultation training and certification
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- why choose docline --}}
        <br>
        <hr>
        <br>
        <div class="d-flex justify-content-center">
            <div class="docline-title text-center">Why choose DocsApp</div>
        </div>
        <br>
        <br>


        <div class="dekhavu">
            <div class="container">
                <div class="row">
                    <ul class="docline nav nav-tabs text-center">


                        <li class="doc nav-item col-sm-2">
                            <a class="nav-link active" data-toggle="tab" href="#firstDoctor">Fastest Doctor <br>
                                Response</a>
                        </li>


                        <li class="doc nav-item col-sm-2">
                            <a class="nav-link " data-toggle="tab" href="#secondDoctor">Doctor 24x7</a>
                        </li>


                        <li class="doc nav-item col-sm-2">
                            <a class="nav-link" data-toggle="tab" href="#thirdDoctor">Specialist Doctor</a>
                        </li>


                        <li class="doc nav-item col-sm-2">
                            <a class="nav-link" data-toggle="tab" href="#fourthDoctor">Private & Secure <br>
                                Consultation</a>
                        </li>


                        <li class="doc nav-item col-sm-2">
                            <a class="nav-link" data-toggle="tab" href="#fifthDoctor">Qulity & Trust</a>
                        </li>


                        <li class="doc nav-item col-sm-2">
                            <a class="nav-link" data-toggle="tab" href="#sixthDoctor">Discounts on Lab & <br>
                                Medicine</a>
                        </li>
                    </ul>
                </div>
            </div>
            {{-- navbar active jquery part --}}
            <script>
                $('document').ready(function () {
                    $('.doc').click(function () {
                        $('.doc').removeClass('active');
                        $(this).addClass('active');
                    });
                });
            </script>
            <br>
            <div class="container">
                <div class="row">
                    <div id="myTabContent" class="tab-content">

                        <div class="tab-pane fade active show col-sm-12" id="firstDoctor">
                            <div class="row container">
                                <div class="col-sm-6">
                                    <div class="fast-doctor text-center">
                                        <img src="/assets/img/fast-doctor.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6 d-flex align-items-center">
                                    <div class="">
                                        <div class="title">Doctor Consultation Starts <br> in less than 30 mins</div>
                                        <div class="sub-title">No appointment booking or travelling, just ask your
                                            health question and get answer from experienced doctors in 30 minutes from
                                            your home
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade col-sm-12" id="secondDoctor">
                            <div class="row container">
                                <div class="col-sm-6">
                                    <div class="fast-doctor text-center">
                                        <img src="/image/docline/clock.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6 d-flex align-items-center">
                                    <div class="">
                                        <div class="title">Doctors are available 24 <br>hours, 7 days a week!</div>
                                        <div class="sub-title">With DocsApp recover quicker than ever. Talk to doctors
                                            all day and all night long, even on holidays.
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane fade col-sm-12" id="thirdDoctor">
                            <div class="row container">
                                <div class="col-sm-6">
                                    <div class="fast-doctor text-center">
                                        <img src="/image/docline/specialist-doctor.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6 d-flex align-items-center justify-content-end">
                                    <div class="">
                                        <div class="title">5000+ doctors are available in over 18 specialties</div>
                                        <div class="sub-title">Best specialist Doctors like Sexologists, Dermatologists,
                                            Gynecologists etc are available on the app ensure that you get the right
                                            treatment at the right time.
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane fade col-sm-12" id="fourthDoctor">
                            <div class="row container">
                                <div class="col-sm-6">
                                    <div class="fast-doctor text-center">
                                        <img src="/image/docline/private.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6 d-flex align-items-center justify-content-end">
                                    <div class="">
                                        <div class="title">Your Consultations are Private & Confidential</div>
                                        <div class="sub-title">You can talk about all your problems with the doctors and
                                            be assured that your chats and calls are private and secure. You can also
                                            delete chats whenever you want.
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane fade col-sm-12" id="fifthDoctor">
                            <div class="row container">
                                <div class="col-sm-6">
                                    <div class="fast-doctor text-center">
                                        <img src="/image/docline/india.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6 d-flex align-items-center justify-content-end">
                                    <div class="">
                                        <div class="title">Consult Best and Trusted Specialist Doctors in India</div>
                                        <div class="sub-title">Only the best of the best doctors are available on
                                            DocsApp, doctors are selected after 3 step verification process. We take
                                            strict actions based on feedback by customers. Trusted by more than 5
                                            million Indians.
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="tab-pane fade col-sm-12" id="sixthDoctor">
                            <div class="row container">
                                <div class="col-sm-6">
                                    <div class="fast-doctor text-center">
                                        <img src="/image/docline/lab.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6 d-flex align-items-center justify-content-end">
                                    <div class="">
                                        <div class="title">Book blood tests or Order Medicines with Discounts</div>
                                        <div class="sub-title">You can book blood tests and order medicines from the
                                            comfort of your home, you can get treated without stepping out of your home.
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    {{-- myTabContent div --}}
                </div>
                {{-- row div --}}
            </div>
            {{-- container div --}}
        </div>

        {{-- collapse  --}}

        <div class="santadvu container">
            <div id="accordion">
                <div class="card text-center">
                    <div class="card-header">
                        <a class="card-link" data-toggle="collapse" href="#collapseOne">
                            Fastest Doctor <br> Response
                        </a>
                    </div>
                    <div id="collapseOne" class="collapse show" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row container">
                                <div class="col-sm-6">
                                    <div class="fast-doctor text-center">
                                        <img src="/image/docline/fast-doctor.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6 d-flex align-items-center">
                                    <div class="">
                                        <div class="title">Doctor Consultation Starts <br> in less than 30 mins</div>
                                        <div class="sub-title">No appointment booking or travelling, just ask your
                                            health question and get answer from experienced doctors in 30 minutes from
                                            your home
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-center">
                    <div class="card-header">
                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
                            Doctor 24x7
                        </a>
                    </div>
                    <div id="collapseTwo" class="collapse" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row container">
                                <div class="col-sm-6">
                                    <div class="fast-doctor text-center">
                                        <img src="/image/docline/clock.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6 d-flex align-items-center">
                                    <div class="">
                                        <div class="title">Doctors are available 24 <br>hours, 7 days a week!</div>
                                        <div class="sub-title">With DocsApp recover quicker than ever. Talk to doctors
                                            all day and all night long, even on holidays.
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card text-center">
                    <div class="card-header">
                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
                            Specialist Doctor
                        </a>
                    </div>
                    <div id="collapseThree" class="collapse" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row container">
                                <div class="col-sm-6">
                                    <div class="fast-doctor text-center">
                                        <img src="/image/docline/specialist-doctor.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6 d-flex align-items-center justify-content-end">
                                    <div class="">
                                        <div class="title">5000+ doctors are available in over 18 specialties</div>
                                        <div class="sub-title">Best specialist Doctors like Sexologists, Dermatologists,
                                            Gynecologists etc are available on the app ensure that you get the right
                                            treatment at the right time.
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="card text-center">
                    <div class="card-header">
                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseFourth">
                            Private & Secure <br> Consultation
                        </a>
                    </div>
                    <div id="collapseFourth" class="collapse" data-parent="#accordion">
                        <div class="card-body">
                            <div class="row container">
                                <div class="col-sm-6">
                                    <div class="fast-doctor text-center">
                                        <img src="/image/docline/private.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6 d-flex align-items-center justify-content-end">
                                    <div class="">
                                        <div class="title">Your Consultations are Private & Confidential</div>
                                        <div class="sub-title">You can talk about all your problems with the doctors and
                                            be assured that your chats and calls are private and secure. You can also
                                            delete chats whenever you want.
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="card text-center">
                    <div class="card-header">
                        <a class="collapsed card-link" data-toggle="collapse" href="#collapseFifth">
                            Qulity & Trust
                        </a>
                    </div>
                    <div id="collapseFifth" class="collapse" data-parent="#accordion">
                        <div class="row container">
                            <div class="col-sm-6">
                                <div class="fast-doctor text-center">
                                    <img src="/image/docline/india.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-sm-6 d-flex align-items-center justify-content-end">
                                <div class="">
                                    <div class="title">Consult Best and Trusted Specialist Doctors in India</div>
                                    <div class="sub-title">Only the best of the best doctors are available on DocsApp,
                                        doctors are selected after 3 step verification process. We take strict actions
                                        based on feedback by customers. Trusted by more than 5 million Indians.
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div class="card text-center">
                <div class="card-header">
                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseSix">
                        Discounts on Lab & Medicine
                    </a>
                </div>
                <div id="collapseSix" class="collapse" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row container">
                            <div class="col-sm-6">
                                <div class="fast-doctor text-center">
                                    <img src="/image/docline/lab.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-sm-6 d-flex align-items-center justify-content-end">
                                <div class="">
                                    <div class="title">5000+ doctors are available in over 18 specialties</div>
                                    <div class="sub-title">Best specialist Doctors like Sexologists, Dermatologists,
                                        Gynecologists etc are available on the app ensure that you get the right
                                        treatment at the right time.
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>


        <div class="mid3">
            <p>Meet Our Founders</p>
        </div>

        <!-- part5 starts  -->
        <div class="part5">
            <div class="row">

                <div class="col-md-4 ml-auto">
                    <div class="card">
                        <div class="card-header">
                            <img src="/assets/img/Mehul.jpg" class="founderimg">
                        </div>
                        <div class="ml-auto mr-auto foundername">
                            <span>Mehul Vamja</span>
                        </div>
                        <div class="ml-auto mr-auto foundertext">
                            <span>Frontend Developer, Doc-Line</span>
                        </div>
                        <div class="mr-auto ml-auto founderdetail">
                            <span>Mehul conceptualized the idea of Doc-Line after realising that appointment booking is a difficult task for person who is busy in their work. Hence, with the help of new technology.</span>
                        </div>
                        <br>
                    </div>
                </div>

                <div class="col-md-4 mr-auto">
                    <div class="card">
                        <div class="card-header">
                            <img src="/assets/img/Pujan.jpg" class="founderimg">
                        </div>
                        <div class="ml-auto mr-auto foundername">
                            <span>Pujan Vachhani</span>
                        </div>
                        <div class="ml-auto mr-auto foundertext">
                            <span>Backend Developer, Doc-Line</span>
                        </div>
                        <div class="mr-auto ml-auto founderdetail">
                            <span class="pujan">Having experience in the development of healthcare devices, Pujan aims to establish Doc-Line as a trustworthy brand in healthcare serving millions of lives with the help of innovative tech-driven solution.</span>
                        </div>
                        <br>
                    </div>
                </div>

            </div>
        </div>
        <!-- part5 ends  -->

        <br>


        <!-- part6 starts  -->
        <div class="part6">
            <div class="row">
                <div class="col-md-5 ml-auto">
                    <img src="/image/team.jpg" height="300px" width="500px" class="teamimg">
                </div>
                <div class="col-md-5 mr-auto team">
                    <p class="teamtext1">Our Team</p>

                    <p class="teamtext2">We are a bunch of hardworking folks working towards making world-class
                        healthcare
                        affordable to the global population. We are serious about work days and even more serious about
                        weekends. Heard of round pegs in square holes? We are those pegs, challenging the status quo in
                        healthcare all day, every day.
                    </p>

                    <button class="btn">Join us</button>
                </div>
            </div>
        </div>
        <!-- part6 ends  -->

        <br>
        <!-- part7 starts  -->
        <div class="part7">
            <div class="last row">
                <div class="ml-auto col-md-5 feedback">
                    <span class="lastname">Your Feedback</span>
                    <br>
                    <span class="lasttext">We would like to here your comments about our website. By filling this feedback form you can tell us your thoughts. If you face any difficulty then please let us know about it. So we can fix it and give you a good service.</span>

                </div>
                <div class="form col-md-4 mr-auto">
                    <div class="form-group">
                        <input type="text" name="name" class="name" placeholder="Your Name">
                        <input type="text" name="email" class="email" placeholder="Your Email">
                        <br><br>
                        <textarea type="text" name="msg" class="msg" placeholder="Your Message"></textarea>
                        <br><br>
                        <input type="submit" name="btn" class="btn">
                    </div>
                </div>
            </div>
            <hr class="">
            <div class="copyright">
                <span class="lastline">&copy; Copyright 2019. Docline Private Ltd.</span>
                <span class="lastlink"><a href="#">Terms and Conditions & Privacy Policy</a></span>
            </div>
            <br>
        </div>
    </div>
    <!-- part7 ends  -->
</div>

</body>
</html>
